from WGANGP import GAN
import argparse
import configs
import numpy as np
import matplotlib.pyplot as plt
import os
import tensorflow as tf
import pickle
from joblib import load

#TODO dynamic arg
opts = configs.config_awake_small

zfile = "zlatent_01.np"

min_loss_delta = 0.0000000001

def main():
    parser = argparse.ArgumentParser(description='Find latent representation of reference images using perceptual loss')
    parser.add_argument('--src_dir', default='data/ref_images', help='Directory with images for encoding')
    parser.add_argument('--generated_images_dir', default='generated_images', help='Directory for storing generated images')
    parser.add_argument('--dlatent_dir',  default='latent_representations', help='Directory for storing dlatent representations')
    parser.add_argument('--run_dir',  default='run/2020-08-20_14-36-13/', help='Directory containing trained generator model')

    # Gradient descent parameters
    parser.add_argument('--iterations', default=100, help='Number of optimization steps for each batch', type=int)
    parser.add_argument('--lr', default=0.99, help='Starting learning rate', type=float)

    args, other_args = parser.parse_known_args()

    iterations = args.iterations
    run_folder = args.run_dir
    pfn = run_folder + 'weights/weights.h5'

    ref_images = 'data/ref_images/ref_01.pkl'
    ref_image_recovered_z = 'data/ref_images/ref_01_recovered_z'

    os.makedirs(args.generated_images_dir, exist_ok=True)
    os.makedirs(args.dlatent_dir, exist_ok=True)

    with open(os.path.join(run_folder, 'params.pkl'), 'rb') as f:
        opts = pickle.load (f)

    if opts['input_normalize_sym']:
        scaler = load(run_folder + 'scaler.bin')

    with open(ref_images, 'rb') as f:
        ref_img = pickle.load(f)

    if opts['input_normalize_sym']:
        print('normalising')
        ref_img_norm = scaler.transform(ref_img.reshape(-1, opts['img_dim'][0] * opts['img_dim'][1])
                                   ).reshape(opts['img_dim'][0], opts['img_dim'][1])
    else:
        ref_img_norm = ref_img

    # Initialize generator and perceptual model
    graph = tf.Graph()

    with graph.as_default():
        sess = tf.compat.v1.Session(graph=graph)
        with sess.as_default():

            gan = GAN(opts)
            gan.load_weights(pfn)
            generator = gan.generator
            z_dim = gan.z_dim

            print('z dimension : ', z_dim)

            fz = tf.Variable(ref_img_norm, tf.float32)
            fz = tf.expand_dims(fz, 0)
            fz = tf.cast(fz, tf.float32)

            zp0 = np.random.normal(size=(1, z_dim))
            zp = tf.Variable(zp0, dtype=tf.float32)

            # Define the optimization problem
            fzp = generator(zp)

            loss = tf.losses.mean_squared_error(labels=fz, predictions=fzp)

            # Decayed gradient descent
            global_step = tf.Variable(0, trainable=False)
            starter_learning_rate = tf.Variable(args.lr, trainable=False)
            learning_rate = tf.train.exponential_decay(starter_learning_rate,
                                                       global_step,
                                                       100, 0.99)

            opt = tf.train.GradientDescentOptimizer(learning_rate)

            # Optimize on the variable zp
            train = opt.minimize(loss, var_list=zp, global_step=global_step)

            init_op = tf.initialize_all_variables()
            sess.run(init_op)

            gan.load_weights(pfn)
            generator = gan.generator

            old_loss = 1e10

            for i in range(iterations):  # Use more iterations (10000)
                # If we know the original latent vector, we can also compute
                # how far the recovered vector is from it
                _, loss_value, zp_val, eta = sess.run((train, loss, zp, learning_rate))
                if i % 10 == 0:
                    print("%03d) eta=%03f, loss = %f" % (i, eta, loss_value))
                if old_loss - loss_value < min_loss_delta:
                    print("exiting on loss delta")
                    print("%03d) eta=%03f, loss = %f" % (i, eta, loss_value))
                    break
                old_loss = loss_value

            # Save the recovered latent vector
            zp_val = zp.eval()
            np.save(os.path.join(run_folder, zfile), zp_val)

            # plot the recovered image and compare to the original
            gen_imgs = generator(zp).eval().squeeze()

            if opts['input_normalize_sym']:
                print('un-normalising')
                gen_imgs = scaler.inverse_transform(
                    gen_imgs.reshape(opts['img_dim'][0] * opts['img_dim'][1])).reshape(
                    opts['img_dim'][0], opts['img_dim'][1])

            r, c = 2, 2
            fig, axs = plt.subplots(r, c, figsize=(8, 8))
            cnt = 0
            for i in range(r):
                for j in range(c):
                    axs[i, j].plot(np.squeeze(gen_imgs[:,cnt]),label='generated')
                    axs[i, j].plot(np.squeeze(ref_img[:,cnt]), ':', label='original')
                    # axs[i, j].axis('off')
                    # axs[i, j].axis('tight')
                    cnt += 1
                    axs[i, j].legend(loc='upper right')
            plt.savefig(os.path.join(args.generated_images_dir,'tf_sample_01.png'))
            plt.close()

if __name__ == "__main__":
    main()