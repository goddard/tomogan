from WGANGP import GAN
import argparse
import numpy as np
import matplotlib.pyplot as plt
import os
import pickle
from joblib import load
from utils import stochasticClip
import numpy as np

from pymoo.model.problem import Problem
from pymoo.algorithms.so_de import DE
from pymoo.algorithms.so_genetic_algorithm import GA
from pymoo.algorithms.nsga2 import NSGA2
from pymoo.algorithms.so_nelder_mead import NelderMead
from pymoo.algorithms.so_brkga import BRKGA
from pymoo.algorithms.so_cmaes import CMAES

from pymoo.factory import get_sampling, get_crossover, get_mutation
from pymoo.factory import get_termination
from pymoo.factory import get_problem
from pymoo.util.normalization import denormalize
from pymoo.optimize import minimize
from pymoo.operators.sampling.latin_hypercube_sampling import LatinHypercubeSampling

class MyProblem(Problem):

    def __init__(self, zdim, generator, ref_img_norm, scamplitude):
        super().__init__(n_var=zdim,
                         n_obj=1,
                         n_constr=0,
                         xl = -3.5 * np.ones(zdim),
                         xu =  3.5 * np.ones(zdim))
        self.generator = generator
        self.ref_img_norm = ref_img_norm
        self.scamplitude = scamplitude

    def _evaluate(self, x, out, *args, **kwargs):
        if self.scamplitude > 0:
            x = stochasticClip(x, self.scamplitude)

        img_1 = self.generator.predict(x).squeeze()

        if x.shape[0] > 1:
            err = np.asarray([MSE(self.ref_img_norm, img_1[k, :, :]) for k in range(x.shape[0])])
        else:
            err = np.asarray([MSE(self.ref_img_norm, img_1)])
        f1 = err
        g1 = -err # TODO sort out g1 properly

        out["F"] = f1
        out["G"] = g1

def MSE(img1, img2):
    img1 = img1.flatten()
    img2 = img2.flatten()
    squared_diff = (img1 - img2) ** 2
    summed = np.sum(squared_diff)
    return summed

def main():
    parser = argparse.ArgumentParser(description='Find latent representation of reference images using perceptual loss')
    parser.add_argument('--src_dir', default='data/ref_images', help='Directory with images for encoding')
    parser.add_argument('--generated_images_dir', default='generated_images', help='Directory for storing generated images')
    parser.add_argument('--dlatent_dir',  default='latent_representations', help='Directory for storing dlatent representations')
    parser.add_argument('--run_dir',  default='run/2020-08-20_14-36-13/', help='Directory containing trained generator model')

    # Algorithm choice
    parser.add_argument('--algorithm', default='CMAES', help='Algorithm choice', type=str)

    # Genetic algorithm parameters
    parser.add_argument('--iter', default=100, help='Number of iterations', type=int)
    parser.add_argument('--gen', default=13, help='Number of generations', type=int)
    parser.add_argument('--pop', default=100, help='Population per generation', type=int)
    parser.add_argument('--CR', default=0.5, help='Exchange probability', type=float)
    parser.add_argument('--F', default=0.3, help='Crossover_weight', type=float)
    parser.add_argument('--scamplitude', default=0.0, help='Stochastic clipping amplitude', type=float)
    parser.add_argument('--restarts', default=5, help='Restarts', type=int)
    args, other_args = parser.parse_known_args()

    run_folder = args.run_dir
    pfn = run_folder + 'weights/weights.h5'

    ref_images = 'data/ref_images/ref_01.pkl'
    ref_image_recovered_z = 'data/ref_images/' + args.algorithm + 'ref_01_recovered_z'

    os.makedirs(args.generated_images_dir, exist_ok=True)
    os.makedirs(args.dlatent_dir, exist_ok=True)

    with open(os.path.join(run_folder, 'params.pkl'), 'rb') as f:
        opts = pickle.load (f)

    if opts['input_normalize_sym']:
        scaler = load(run_folder + 'scaler.bin')

    with open(ref_images, 'rb') as f:
        ref_img = pickle.load(f)

    if opts['input_normalize_sym']:
        print('normalising')
        ref_img_norm = scaler.transform(ref_img.reshape(-1, opts['img_dim'][0] * opts['img_dim'][1])
                                   ).reshape(opts['img_dim'][0], opts['img_dim'][1])
    else:
        ref_img_norm = ref_img

    gan = GAN(opts)
    gan.load_weights(pfn)
    generator = gan.generator
    z_dim = gan.z_dim
    print('z dimension : ', z_dim)

    problem = MyProblem(z_dim, generator, ref_img_norm, args.scamplitude)

    if args.algorithm == 'DE':
        algorithm = DE(
            pop_size=args.pop,
            sampling=LatinHypercubeSampling(iterations=args.iter, criterion="maxmin"),
            variant="DE/rand/1/bin",
            CR=args.CR,
            F=args.F,
            dither="vector",
            jitter=False
        )

    elif args.algorithm == 'GA':
        algorithm = GA(
            pop_size=args.pop,
            sampling=LatinHypercubeSampling(iterations=args.iter, criterion="maxmin"),
            crossover=get_crossover("real_two_point"),
            mutation=get_mutation("real_pm"),
            eliminate_duplicates=True)

    elif args.algorithm == 'NSGA2':
        algorithm = NSGA2(pop_size=args.pop,
            sampling=LatinHypercubeSampling(iterations=args.iter, criterion="maxmin"),
            crossover=get_crossover("real_two_point"),
            mutation=get_mutation("real_pm"),
            eliminate_duplicates=True)

    elif args.algorithm == 'NelderMead':
        algorithm = NelderMead()

    elif args.algorithm == 'BRKGA':
        algorithm = BRKGA(
            n_elites=200,
            n_offsprings=700,
            n_mutants=100,
            bias=0.7)

    elif args.algorithm == 'CMAES':
        algorithm = CMAES(
            sigma=0.5,
            cmaes_verbose=-1,
            restarts = args.restarts,
            restart_from_best = True)

    termination = get_termination("n_gen", args.gen)

    res = minimize(problem,
                   algorithm,
                   termination,
                   seed=99,
                   verbose=False,
                   save_history=False)

    print("Best solution found: \nX = %s\nF = %s" % (res.X, res.F))

    gen_imgs = generator.predict(res.X.reshape(1, z_dim))

    if opts['input_normalize_sym']:
        print('un-normalising')
        gen_imgs = scaler.inverse_transform(
            gen_imgs.reshape(opts['img_dim'][0] * opts['img_dim'][1])).reshape(
            opts['img_dim'][0], opts['img_dim'][1])

    r, c = 2, 2
    fig, axs = plt.subplots(r, c, figsize=(8, 8))
    fig.suptitle('Loss ' + str(res.F[0]))
    cnt = 0
    for i in range(r):
        for j in range(c):
            axs[i, j].plot(np.squeeze(gen_imgs[:,cnt]),label='generated')
            axs[i, j].plot(np.squeeze(ref_img[:,cnt]), ':', label='original')
            cnt += 1
            axs[i, j].legend(loc='upper right')
    plt.savefig(os.path.join(args.generated_images_dir, args.algorithm + '_sample_01.png'))
    plt.close()

    np.save(ref_image_recovered_z, res.X)

    # import matplotlib.pyplot as plt
    # from pymoo.performance_indicator.hv import Hypervolume
    #
    # # create the performance indicator object with reference point (4,4)
    # metric = Hypervolume(ref_point=np.array([1.0, 1.0]))
    #
    # # collect the population in each generation
    # pop_each_gen = [a.pop for a in res.history]
    #
    # # receive the population in each generation
    # obj_and_feasible_each_gen = [pop[pop.get("feasible")[:, 0]].get("F") for pop in pop_each_gen]
    #
    # # calculate for each generation the HV metric
    # hv = [f for f in obj_and_feasible_each_gen]
    #
    # # visualze the convergence curve
    # minf = []
    # avgf = []
    # for i in range(len(hv)):
    #     minf.append(np.min(hv[i]))
    #     avgf.append(np.mean(hv[i]))
    # plt.plot(minf, '-o', label = 'best F')
    # plt.plot(avgf, '--', label = 'average F')
    # plt.legend()
    # plt.title("Convergence")
    # plt.xlabel("Generation")
    # plt.ylabel("Loss Function")
    # plt.savefig(os.path.join(args.generated_images_dir, args.algorithm + '_training.png'))
    # plt.close()

if __name__ == "__main__":
    main()