import os
import tensorflow as tf
import numpy as np


def stochasticClip(x, SCamplitude):
    while np.max(x) > SCamplitude:
        ind = np.unravel_index(np.argmax(x, axis=None), x.shape)
        x[ind] = x[ind] - np.random.normal(loc=0.3, scale=0.1)
    while -np.min(x) > SCamplitude:
        ind = np.unravel_index(np.argmin(x, axis=None), x.shape)
        x[ind] = x[ind] + np.random.normal(loc=0.3, scale=0.1)

    return x

def o_gfile(filename, mode):
    """Wrapper around file open, using gfile underneath.

    filename can be a string or a tuple/list, in which case the components are
    joined to form a full path.
    """
    if isinstance(filename, tuple) or isinstance(filename, list):
        filename = os.path.join(*filename)
    return File(filename, mode)

class File(tf.gfile.GFile):
    """Wrapper on GFile extending seek, to support what python file supports."""
    def __init__(self, *args):
        super(File, self).__init__(*args)

    def seek(self, position, whence=0):
        if whence == 1:
            position += self.tell()
        elif whence == 2:
            position += self.size()
        else:
            assert whence == 0
        super(File, self).seek(position)

class Conv1DTranspose(tf.keras.layers.Layer):
    def __init__(self, filters, kernel_size, strides=1, padding='valid'):
        super().__init__()
        self.conv2dtranspose = tf.keras.layers.Conv2DTranspose(
          filters, (kernel_size, 1), (strides, 1), padding
        )

    def call(self, x):
        x = tf.expand_dims(x, axis=2)
        x = self.conv2dtranspose(x)
        x = tf.squeeze(x, axis=2)
        return x

