# simulate images analytically to test GAN

import numpy as np
import pickle
import matplotlib.pyplot as plt

class FakeImgGen:
    """ To centre and crop image both in VAE training and encoding
    """
    def __init__(self, img_shape, maxpeaks, sigrange, murange, arange, noise, plot = False):
        self.img_shape = img_shape
        self.maxpeaks = maxpeaks
        self.sigrange = sigrange
        self.murange = murange
        self.arange = arange
        self.noise = noise
        self.max_int = 0
        self.plot = plot

    def _getRands(self, _npeaks):
        #generate random mux
        mux = np.zeros((_npeaks, 2))
        mux[0] = [self.img_shape[0] / 2]
        mus = np.random.uniform(low = self.murange[0], high = self.murange[1], size = (_npeaks))
        mux = mus

        #generate random sigx
        sigx = np.random.uniform(low = self.sigrange[0], high = self.sigrange[1], size = (_npeaks))

        #generate random amplitude (with a[0] = 1)
        a = np.ones((_npeaks))
        if _npeaks > 2:
            arands = np.random.uniform(low = self.arange[0], high = self.arange[1], size = (_npeaks - 1,1)).squeeze()
        else:
            arands = np.random.uniform(low = self.arange[0], high = self.arange[1])
        a[1::] = arands

        return(mux, sigx, a)

    def _oneD_Gaussian(self, _amplitude, _x0, _sigma_x, _offset = 0):
        x = np.linspace(0, self.img_shape[0] - 1, self.img_shape[0])
        _x0 = float(_x0)
        a = 1/(2*_sigma_x**2)

        g = _offset + _amplitude * np.exp( - (a*((x - _x0)**2)))

        _data = g.ravel()
        _data = _data + self.noise * np.random.normal(size=(self.img_shape[0]))
        return (_data)

    def _plotData(self, j):
        # plot oneD_Gaussian data
        plt.plot(self.data)
        plt.title('projection ' + str(j))
        plt.show()

    def step(self, j):
        self.data = np.zeros(self.img_shape)
        _npeaks = np.random.randint(low = 1, high = self.maxpeaks + 1)
        for k in range(self.img_shape[1]):
            params = self._getRands(_npeaks)
            for i in range(_npeaks):
                _x0 = params[0][i]
                _sigma_x = params[1][i]
                _amplitude = params[2][i]
                _data = self._oneD_Gaussian(_amplitude, _x0, _sigma_x)
                self.data[:,k] = self.data[:,k] + _data
            self.data[:, k] = np.clip(self.data[:, k], 0, np.max(self.data[:, k]))
            self.data[:, k] = np.divide(self.data[:, k], np.sum(self.data[:, k]))
        if self.plot:
            self._plotData(j)

        return(self.data)


def main():

    nimgs = 10000

    planes = 4
    bins = 256

    img_shape = [bins, planes]
    maxpeaks = 4
    sigrange = [4, 40]
    murange = [50, 206]
    arange = [0.2, 1.0]
    noise = 0.01

    out_file = 'data/test_set_05.pkl'

    imgGen = FakeImgGen(img_shape, maxpeaks, sigrange, murange, arange, noise)

    imgs = []
    for j in range(nimgs):
        img = imgGen.step(j)
        if j == 100:
            with open('data/ref_images/ref_01.pkl', 'wb') as f:
                pickle.dump(img, f)
            plt.plot(img)
            plt.show()

        imgs.append(img)
    x = np.array(imgs)
    print(x.shape)

    with open(out_file, 'wb') as handle:
        pickle.dump(x, handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    main()

