# simulate images analytically to test GAN

import numpy as np
import pickle
import matplotlib.pyplot as plt

class FakeImgGen:
    """ To make a pdf of n Gaussians then sample a dataset from the corresponding pdf
    """
    def __init__(self, img_shape, maxpeaks, xlim, ylim, sigrange, murange, arange, tiltrange, noise, plot, save_fig, outdir):
        self.img_shape = img_shape
        self.maxpeaks = maxpeaks
        self.xlim = xlim
        self.ylim = ylim
        self.sigrange = sigrange
        self.murange = murange
        self.arange = arange
        self.tiltrange = tiltrange
        self.noise = noise
        self.plot = plot
        self.out_dir = out_dir
        self.max_int = 0


    def _samplePdf(self):
        lo = np.min(self.xlim, self.ylim)
        hi = np.max(self.xlim,self.lim)
        Uxy = np.random.uniform(lo, hi, size=(10000, 3))
        for u in Uxy:
            if 0.001 * self.PDF[u[0], u[1], u[2]] > np.random.random():
                return u
        return 'err'

    def _getRands(self, _npeaks):
        #generate random mux,y (with mux,y[0] centered)
        muxy = np.zeros((_npeaks, 2))
        muxy[0] = [self.img_shape[0] / 2, self.img_shape[1] / 2]
        mus = np.random.uniform(low = self.murange[0], high = self.murange[1], size = (_npeaks, 2))
        # always centred on muxy[0]
        muxy[1::] = mus[1::]

        #generate random sigx,y
        sigxy = np.random.uniform(low = self.sigrange[0], high = self.sigrange[1], size = (_npeaks, 2))

        #generate random amplitude (with a[0] = 1)
        a = np.ones((_npeaks))
        if _npeaks > 2:
            arands = np.random.uniform(low = self.arange[0], high = self.arange[1], size = (_npeaks - 1,1)).squeeze()
        else:
            arands = np.random.uniform(low = self.arange[0], high = self.arange[1])
        a[1::] = arands

        #generate random tilts
        if _npeaks > 1:
            tilt = np.random.uniform(low = 0.0, high = self.tiltrange, size = (_npeaks,1)).squeeze()
        else:
            tilt = np.random.uniform(low = 0.0, high = self.tiltrange)

        return(muxy, sigxy, a, tilt)

    def _twoD_Gaussian(self, xdata_tuple, amplitude, xo, yo, sigma_x, sigma_y, theta, offset = 0):
        (x, y) = xdata_tuple
        xo = float(xo)
        yo = float(yo)
        a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
        b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)
        c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
        g = offset + amplitude*np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo)
                            + c*((y-yo)**2)))
        return g.ravel()

    def _makeGrid(self, _amplitude, _xo, _yo, _sigma_x, _sigma_y, _theta):
        x = np.linspace(0, self.img_shape[0] - 1, self.img_shape[0])
        y = np.linspace(0, self.img_shape[1] - 1, self.img_shape[1])
        x, y = np.meshgrid(x, y)
        xdata_tuple = (x, y)

        #create data
        _data = self._twoD_Gaussian(xdata_tuple, _amplitude, _xo, _yo, _sigma_x, _sigma_y, _theta)
        _data = _data + self.noise*np.random.normal(size=(self.img_shape[0] * self.img_shape[1]))
        return(_data)

    def _plotData(self, j):
        # plot twoD_Gaussian data generated above
        plt.ioff()
        fig = plt.figure()
        plt.imshow(self.data.reshape(self.img_shape[0], self.img_shape[1]))
        plt.axis('off')
        plt.axis('tight')
        if self.save_fig:
            plt.savefig(self.out_dir + '/fig_' + str(j) + '.png')
        if self.plot:
            plt.show()
        plt.close(fig)

    def step(self, j):
        self.data = self._makeGrid(0, 0, 0, 1, 1, 1) # noise floor
        _npeaks = np.random.randint(low = 0, high = self.maxpeaks + 1)
        if _npeaks > 0:
            params = self._getRands(_npeaks)
            for i in range(_npeaks):
                _xo = params[0][i][0]
                _yo = params[0][i][1]
                _sigma_x = params[1][i][0]
                _sigma_y = params[1][i][1]
                _amplitude = params[2][i]
                _theta = np.deg2rad(params[3][i])
                _data = self._makeGrid(_amplitude, _xo, _yo, _sigma_x, _sigma_y, _theta)
                self.data = self.data + _data
        self._plotData(j)

        return(self.data)


def main():

    nimgs = 10000

    planes = 2
    bins = 28
    img_shape = [bins, bins, planes]
    maxpeaks = 4
    sigrange = [2, 5]
    murange = [10, 17]
    arange = [0.2, 1.0]
    tiltrange = [0, 45]
    noise = 0.05
    plot = True
    save_fig = True
    out_dir = '/content'

    out_file = 'data/cgan_test_set_01.pkl'

    imgGen = FakeImgGen(img_shape, maxpeaks, sigrange, murange, arange, tiltrange, noise, plot, save_fig, out_dir)

    imgs = []
    for j in range(nimgs):
        img = imgGen.step(j)
        imgs.append(img)
    x = np.array(imgs)
    print(x.shape)

    with open(out_file, 'wb') as handle:
        pickle.dump(x, handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    main()

