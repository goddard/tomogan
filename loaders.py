import pickle
import os
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from joblib import dump


def load_model(model_class, folder):
    with open(os.path.join(folder, 'params.pkl'), 'rb') as f:
        params = pickle.load(f)

    model = model_class(*params)

    model.load_weights(os.path.join(folder, 'weights/weights.h5'))

    return model


def load_model_pararms(model_class, folder, param_file):
    with open(os.path.join(folder, param_file), 'rb') as f:
        params = pickle.load(f)

    model = model_class(*params)

    model.load_weights(os.path.join(folder, 'weights/weights.h5'))

    return model
    
def load_model_pararms_build(model_class, folder, param_file):
    with open(os.path.join(folder, param_file), 'rb') as f:
        params = pickle.load(f)

    model = model_class(*params)

    return model


class DataHandler(object):
    """A class storing and manipulating the dataset.

    Assumes that the data is a 3 dimensional array,
    for example [2, 256, 16] is:
        2 distributions per set,
        256 points per distributions,
        16 sets of distributions,
     The shape is contained in self.data_shape
    """

    def __init__(self, opts, run_folder):
        self.data_shape = None
        self.num_points = None
        self.data = None
        self.test_data = None
        self.labels = None
        self.test_labels = None
        self.run_folder = run_folder
        self._load_data(opts)

    def _load_data(self, opts):
        """Load a dataset and fill all the necessary variables.

        """
        if opts['dataset'] == 'awake':
            self._load_awake(opts)
        elif opts['dataset'] == 'test':
            self._load_test(opts)
        else:
            raise ValueError('Unknown %s' % opts['dataset'])

        sym_applicable = ['awake', 'test']

        if opts['input_normalize_sym'] and opts['dataset'] not in sym_applicable:
            raise Exception('Can not normalise this dataset')

        if opts['input_normalize_sym'] and opts['dataset'] in sym_applicable:
            # Normalize data to [-1, 1]
            if isinstance(self.X, np.ndarray):
                scaler = StandardScaler()
                scaler.fit(self.X.reshape(self.X.shape[0],self.X.shape[1]*self.X.shape[2]))
                self.data = scaler.transform(self.data.reshape(self.data.shape[0],self.data.shape[1]*self.data.shape[2])).reshape(self.data.shape[0],self.data.shape[1],self.data.shape[2])
                self.test_data = scaler.transform(self.test_data.reshape(self.test_data.shape[0],self.test_data.shape[1]*self.test_data.shape[2])).reshape(self.test_data.shape[0],self.test_data.shape[1],self.test_data.shape[2])
                with open(os.path.join(self.run_folder, 'scaler.bin'), 'wb') as handle:
                    dump(scaler, handle, compress=True)
                # self.data = (self.data / np.max(self.X) - 0.5) * 2.
                # self.test_data = (self.test_data / np.max(self.X) - 0.5) * 2.


    def _load_awake(self, opts):
        """Load data from AWAKE files test_01.

        """
        _df = opts['data_file']

        with open(_df, 'rb') as f:
            X = pickle.load(f)

        print('opened data file with shape ',X.shape)

        test_size = int(0.2 * X.shape[0])

        tr_X, te_X = train_test_split(X, test_size=test_size)

        self.X = np.concatenate((tr_X, te_X), axis=0)

        seed = 123
        np.random.seed(seed)
        np.random.shuffle(self.X)

        self.data = self.X[:-test_size]
        self.data_shape = self.data.shape

        self.test_data = self.X[-test_size:]
        self.num_points = self.data_shape[0]