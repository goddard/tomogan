from WGANGP import GAN
import argparse
import configs
import numpy as np
import matplotlib.pyplot as plt
import os
import pickle
from joblib import load

zfile = "zlatent_01.np"

alpha = 0.95  # amount by which diversity decays if no better child found
beta = 1.05  # amount by which diversity increases if better child found
diversity = 0.1  # random sigma added to parent
diversity_min = 0.00001  # truncation of episode
sclip = 2.0  # z stochastic clip

num_kids_init = 32768
num_kids_min = 2048

def MSE(img1, img2):
    img1 = img1.flatten()
    img2 = img2.flatten()
    squared_diff = (img1 - img2) ** 2
    summed = np.sum(squared_diff)
    return summed

def main():
    parser = argparse.ArgumentParser(description='Find latent representation of reference images using perceptual loss')
    parser.add_argument('--src_dir', default='data/ref_images', help='Directory with images for encoding')
    parser.add_argument('--generated_images_dir', default='generated_images', help='Directory for storing generated images')
    parser.add_argument('--dlatent_dir',  default='latent_representations', help='Directory for storing dlatent representations')
    parser.add_argument('--run_dir',  default='run/2020-08-25_11-59-41/', help='Directory containing trained generator model')

    # Perceptual model params
    parser.add_argument('--iterations', default=100, help='Number of optimization steps for each batch', type=int)

    # Generator params
    parser.add_argument('--start_sigma', default=0.0, help='Initial sigma of latent z vector (normal, mu = 0.0)', type=bool)
    args, other_args = parser.parse_known_args()

    epochs = args.iterations
    start_sigma = args.start_sigma
    run_folder = args.run_dir
    pfn = run_folder + 'weights/weights.h5'

    ref_images = 'data/ref_images/ref_01.pkl'
    ref_image_recovered_z = 'data/ref_images/ref_01_recovered_z'

    os.makedirs(args.generated_images_dir, exist_ok=True)
    os.makedirs(args.dlatent_dir, exist_ok=True)

    with open(os.path.join(run_folder, 'params.pkl'), 'rb') as f:
        opts = pickle.load (f)

    if opts['input_normalize_sym']:
        scaler = load(run_folder + 'scaler.bin')

    with open(ref_images, 'rb') as f:
        ref_img = pickle.load(f)

    if opts['input_normalize_sym']:
        print('normalising')
        ref_img_norm = scaler.transform(ref_img.reshape(-1, opts['img_dim'][0] * opts['img_dim'][1])
                                   ).reshape(opts['img_dim'][0], opts['img_dim'][1])
    else:
        ref_img_norm = ref_img

    gan = GAN(opts)
    gan.load_weights(pfn)
    generator = gan.generator
    z_dim = gan.z_dim
    print('z dimension : ', z_dim)

    z = np.random.normal(loc=0.0, scale=start_sigma, size=(1, z_dim))

    num_kids = num_kids_init
    run_diversity = diversity
    #child = 1

    img_1 = generator.predict(z).squeeze()

    err = MSE(ref_img_norm, img_1)
    print('image initial error {}'.format(err))

    for j in range(epochs):
        zchild = z + np.random.normal(loc=0.0, scale=run_diversity, size=(num_kids, z_dim))

        # do stochastic clipping
        for i, kid in enumerate(zchild):
            z_clipped = []
            for zval in kid:
                if abs(zval) > sclip:
                    z_clipped.append(np.random.normal(loc=0.0, scale=1))
                else:
                    z_clipped.append(zval)
            zchild[i] = z_clipped

        imgbatch = generator.predict(zchild)

        # find best child
        referr = [MSE(ref_img_norm, imgbatch[k, :, :]) for k in range(num_kids)]
        bestchilderr = np.min(referr)
        if bestchilderr < err:
            err = bestchilderr
            z = zchild[np.argmin(referr)]
            child = np.argmin(referr) + 1
            zp = z.squeeze()
            run_diversity = run_diversity * beta
            print('Epoch {}: min error is {} for child {}, diversity {}'
                  .format(j, round(err, 5), child, round(run_diversity, 5)))
        else:
            run_diversity = alpha * run_diversity
            if num_kids > num_kids_min:
                num_kids = int(num_kids / 2)
            if run_diversity < diversity_min:
                break
            print('Epoch {}: Min error is {}. No better child, keeping z parent, diversity {}, kids {}'
                  .format(j, round(bestchilderr, 5), round(run_diversity, 5), num_kids))

    zp = zp.reshape(1, zp.shape[0])
    gen_imgs = generator.predict(zp)

    endloss = MSE(ref_img_norm, gen_imgs)

    if opts['input_normalize_sym']:
        print('un-normalising')
        gen_imgs = scaler.inverse_transform(
            gen_imgs.reshape(opts['img_dim'][0] * opts['img_dim'][1])).reshape(
            opts['img_dim'][0], opts['img_dim'][1])

    r, c = 2, 2
    fig, axs = plt.subplots(r, c, figsize=(8, 8))
    fig.suptitle('Loss ' + str(endloss))
    cnt = 0
    for i in range(r):
        for j in range(c):
            axs[i, j].plot(np.squeeze(gen_imgs[:,cnt]),label='generated')
            axs[i, j].plot(np.squeeze(ref_img[:,cnt]), ':', label='original')
            cnt += 1
            axs[i, j].legend(loc='upper right')
    plt.savefig(os.path.join(args.generated_images_dir, 'genetic_sample_01.png'))
    plt.close()

    np.save(ref_image_recovered_z, zp)

if __name__ == "__main__":
    main()