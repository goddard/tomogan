import configs
import utils
import os
import datetime
import argparse
from loaders import DataHandler
from WGANGP import GAN
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("--exp", default='awake', help='awake_distributions')
parser.add_argument("--input", help='data_file')
parser.add_argument("--epochs", help='num_training_epochs')
parser.add_argument("--plot_every", help='plot_frequency')
FLAGS = parser.parse_args()

def main():
    if FLAGS.exp == 'awake':
        opts = configs.config_awake
    elif FLAGS.exp == 'awake_small':
        opts = configs.config_awake_small

    if FLAGS.input is not None:
        opts['data_file'] = FLAGS.input

    if FLAGS.epochs is not None:
        opts['epochs'] = int(FLAGS.epochs)

    if FLAGS.plot_every is not None:
        opts['print_every_n_batches'] = int(FLAGS.plot_every)

    run_folder = os.path.join(opts['work_dir'], datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))

    os.makedirs(run_folder)
    os.mkdir(os.path.join(run_folder, 'viz'))
    os.mkdir(os.path.join(run_folder, 'images'))
    os.mkdir(os.path.join(run_folder, 'weights'))

    # Dumping all the configs to a text file
    with utils.o_gfile((run_folder, 'params.txt'), 'w') as text:
        text.write('Parameters for run directory : %s'% run_folder + os.linesep)

        for key in opts:
            text.write('%s : %s' % (key, opts[key]) + os.linesep)

    # Loading the dataset
    data = DataHandler(opts, run_folder)

    assert data.num_points >= opts['batch_size'], 'Training set too small'
    print('min-max values are : {}, {}'.format(np.min(data.data), np.max(data.data)))

    if opts['mode'] == 'build':

        # Creating WAE model
        gan = GAN(opts)

        # Training WAE
        gan.train(data.data, run_folder)

    elif opts['mode'] == 'load':

        # Do something else
       pass

if __name__ == "__main__":
    main()