from WGANGP import GAN
import argparse
import numpy as np
import matplotlib.pyplot as plt
import os
import pickle
from joblib import load
import numpy as np

from scipy.optimize import fmin_cobyla
import scipy

def main():

    parser = argparse.ArgumentParser(description='Find latent representation of reference images using perceptual loss')
    parser.add_argument('--src_dir', default='data/ref_images', help='Directory with images for encoding')
    parser.add_argument('--generated_images_dir', default='generated_images', help='Directory for storing generated images')
    parser.add_argument('--dlatent_dir',  default='latent_representations', help='Directory for storing dlatent representations')
    parser.add_argument('--run_dir',  default='run/2020-08-20_14-36-13/', help='Directory containing trained generator model')

    # Genetic algorithm parameters
    parser.add_argument('--gen', default=1000, help='Number of generations', type=int)
    args, other_args = parser.parse_known_args()

    run_folder = args.run_dir
    pfn = run_folder + 'weights/weights.h5'


    ref_images = 'data/ref_images/ref_01.pkl'
    ref_image_recovered_z = 'data/ref_images/ref_01_recovered_z_cobyla'

    os.makedirs(args.generated_images_dir, exist_ok=True)
    os.makedirs(args.dlatent_dir, exist_ok=True)

    with open(os.path.join(run_folder, 'params.pkl'), 'rb') as f:
        opts = pickle.load (f)

    if opts['input_normalize_sym']:
        scaler = load(run_folder + 'scaler.bin')

    with open(ref_images, 'rb') as f:
        ref_img = pickle.load(f)

    if opts['input_normalize_sym']:
        print('normalising')
        ref_img_norm = scaler.transform(ref_img.reshape(-1, opts['img_dim'][0] * opts['img_dim'][1])
                                   ).reshape(opts['img_dim'][0], opts['img_dim'][1])
    else:
        ref_img_norm = ref_img

    gan = GAN(opts)
    gan.load_weights(pfn)
    generator = gan.generator
    z_dim = gan.z_dim
    print('z dimension : ', z_dim)

    def MSE(img1, img2):
        img1 = img1.flatten()
        img2 = img2.flatten()
        squared_diff = (img1 - img2) ** 2
        summed = np.sum(squared_diff)
        return summed

    def img_loss(x):
        x = np.reshape(x, (1, -1))
        img = generator.predict(x)
        err = MSE(img, ref_img_norm)
        return err

    # adding bounds in form of constraints
    upper = np.ones(z_dim) * 1.5
    lower = np.ones(z_dim) * -1.5
    cons = []
    for factor in range(z_dim):
        lo = {'type': 'ineq', 'fun': lambda x, lb = lower, i = factor: x[i] - lb}
        up = {'type': 'ineq', 'fun': lambda x, ub = upper, i = factor: ub - x[i]}
        cons.append(lo)
        cons.append(up)


    x = np.random.normal(loc=0.0, scale=1, size=((z_dim)))
    options = {'maxiter': args.gen, 'disp': True}


    soln = scipy.optimize.minimize(img_loss, x, constraints = cons, method = 'COBYLA', options = options)

    print("Best solution found: \nX = %s\nF = %s" % (soln.x, img_loss(soln.x)))

    gen_imgs = generator.predict(soln.x.reshape(1, z_dim))

    if opts['input_normalize_sym']:
        print('un-normalising')
        gen_imgs = scaler.inverse_transform(
            gen_imgs.reshape(opts['img_dim'][0] * opts['img_dim'][1])).reshape(
            opts['img_dim'][0], opts['img_dim'][1])

    r, c = 2, 2
    fig, axs = plt.subplots(r, c, figsize=(8, 8))
    fig.suptitle('Loss ' + str(img_loss(soln.x)))
    cnt = 0
    for i in range(r):
        for j in range(c):
            axs[i, j].plot(np.squeeze(gen_imgs[:,cnt]),label='generated')
            axs[i, j].plot(np.squeeze(ref_img[:,cnt]), ':', label='original')
            cnt += 1
            axs[i, j].legend(loc='upper right')
    plt.savefig(os.path.join(args.generated_images_dir, 'cobyla_sample_01.png'))
    plt.close()

    np.save(ref_image_recovered_z, soln.x)

if __name__ == "__main__":
    main()