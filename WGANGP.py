from keras.layers import Input, Conv1D, Flatten, Dense, Reshape, Activation, \
    BatchNormalization, LeakyReLU, Dropout, UpSampling1D
from utils import Conv1DTranspose
from keras.layers.merge import _Merge
from keras.models import Model
from keras import backend as K
from keras.optimizers import Adam, RMSprop
from keras.utils import plot_model
from keras.initializers import RandomNormal
from functools import partial
from joblib import load
import numpy as np
import os
import pickle
import matplotlib.pyplot as plt
from pathlib import Path

class RandomWeightedAverage(_Merge):
    def __init__(self, batch_size):
        super().__init__()
        self.batch_size = batch_size

    """Provides a (random) weighted average between real and generated image samples"""

    def _merge_function(self, inputs):
        alpha = K.random_uniform((self.batch_size, 1, 1))
        return (alpha * inputs[0]) + ((1 - alpha) * inputs[1])


class GAN():
    def __init__(self, opts):

        self.name = 'gan'

        self.opts = opts

        self.input_dim = opts['img_dim']
        self.critic_conv_filters = opts['critic_conv_filters']
        self.critic_conv_kernel_size = opts['critic_conv_kernel_size']
        self.critic_conv_strides = opts['critic_conv_strides']
        self.critic_batch_norm_momentum = opts['critic_batch_norm_momentum']
        self.critic_activation = opts['critic_activation']
        self.critic_dropout_rate = opts['critic_dropout_rate']
        self.critic_learning_rate = opts['critic_learning_rate']
        self.generator_initial_dense_layer_size = opts['generator_initial_dense_layer_size']
        self.generator_upsample = opts['generator_upsample']
        self.generator_conv_filters = opts['generator_conv_filters']
        self.generator_conv_kernel_size = opts['generator_conv_kernel_size']
        self.generator_conv_strides = opts['generator_conv_strides']
        self.generator_batch_norm_momentum = opts['generator_batch_norm_momentum']
        self.generator_activation = opts['generator_activation']
        self.generator_dropout_rate = opts['generator_dropout_rate']
        self.generator_learning_rate = opts['generator_learning_rate']
        self.optimiser = opts['optimiser']
        self.z_dim = opts['z_dim']
        self.grad_weight = opts['grad_weight']
        self.batch_size = opts['batch_size']
        self.n_critic = opts['n_critic']
        self.epochs = opts['epochs']
        self.print_every_n_batches = opts['print_every_n_batches']

        self.n_layers_critic = len(self.critic_conv_filters)
        self.n_layers_generator = len(self.generator_conv_filters)

        self.weight_init = RandomNormal(mean=0., stddev=0.02)  # 'he_normal' #RandomNormal(mean=0., stddev=0.02)

        self.d_losses = []
        self.g_losses = []
        self.epoch = 0

        self._build_critic()
        self._build_generator()
        self._build_adversarial()

    def gradient_penalty_loss(self, y_true, y_pred, interpolated_samples):
        """
        Computes gradient penalty based on prediction and weighted real / fake samples
        """
        gradients = K.gradients(y_pred, interpolated_samples)[0]

        # compute the euclidean norm by squaring ...
        gradients_sqr = K.square(gradients)
        #   ... summing over the rows ...
        gradients_sqr_sum = K.sum(gradients_sqr,
                                  axis=np.arange(1, len(gradients_sqr.shape)))
        #   ... and sqrt
        gradient_l2_norm = K.sqrt(gradients_sqr_sum)
        # compute lambda * (1 - ||grad||)^2 still for each single sample
        gradient_penalty = K.square(1 - gradient_l2_norm)
        # return the mean as loss over all the batch samples
        return K.mean(gradient_penalty)

    def wasserstein(self, y_true, y_pred):
        return -K.mean(y_true * y_pred)

    def get_activation(self, activation):
        if activation == 'leaky_relu':
            layer = LeakyReLU(alpha=0.2)
        else:
            layer = Activation(activation)
        return layer

    def _build_critic(self):

        ### Critic
        critic_input = Input(shape=self.input_dim, name='critic_input')

        x = critic_input

        for i in range(self.n_layers_critic):

            x = Conv1D(
                filters=self.critic_conv_filters[i]
                , kernel_size=self.critic_conv_kernel_size[i]
                , strides=self.critic_conv_strides[i]
                , padding='same'
                , name='critic_conv_' + str(i)
                , kernel_initializer=self.weight_init
            )(x)

            if self.critic_batch_norm_momentum and i > 0:
                x = BatchNormalization(momentum=self.critic_batch_norm_momentum)(x)

            x = self.get_activation(self.critic_activation)(x)

            if self.critic_dropout_rate:
                x = Dropout(rate=self.critic_dropout_rate)(x)

        x = Flatten()(x)

        # x = Dense(512, kernel_initializer = self.weight_init)(x)
        # x = self.get_activation(self.critic_activation)(x)

        critic_output = Dense(1, activation=None
                              , kernel_initializer=self.weight_init
                              )(x)

        self.critic = Model(critic_input, critic_output)

    def _build_generator(self):

        ### Generator

        generator_input = Input(shape=(self.z_dim,), name='generator_input')

        x = generator_input

        x = Dense(np.prod(self.generator_initial_dense_layer_size), kernel_initializer=self.weight_init)(x)
        if self.generator_batch_norm_momentum:
            x = BatchNormalization(momentum=self.generator_batch_norm_momentum)(x)

        x = self.get_activation(self.generator_activation)(x)

        x = Reshape(self.generator_initial_dense_layer_size)(x)

        if self.generator_dropout_rate:
            x = Dropout(rate=self.generator_dropout_rate)(x)

        for i in range(self.n_layers_generator):

            if self.generator_upsample[i] == 2:
                x = UpSampling1D()(x)
                x = Conv1D(
                    filters=self.generator_conv_filters[i]
                    , kernel_size=self.generator_conv_kernel_size[i]
                    , padding='same'
                    , name='generator_conv_' + str(i)
                    , kernel_initializer=self.weight_init
                )(x)
            else:

                print(self.generator_conv_filters[i], self.generator_conv_kernel_size[i], self.generator_conv_strides[i])
                x = Conv1DTranspose(
                    filters=self.generator_conv_filters[i]
                    , kernel_size=self.generator_conv_kernel_size[i]
                    , strides=self.generator_conv_strides[i]
                    , padding='same'
                )(x)


            if i < self.n_layers_generator - 1:

                if self.generator_batch_norm_momentum:
                    x = BatchNormalization(momentum=self.generator_batch_norm_momentum)(x)

                x = self.get_activation(self.generator_activation)(x)

            else:
                x = Activation('tanh')(x)

        generator_output = x
        self.generator = Model(generator_input, generator_output)

    def get_opti(self, lr):
        if self.optimiser == 'adam':
            opti = Adam(lr=lr, beta_1=0.5)
        elif self.optimiser == 'rmsprop':
            opti = RMSprop(lr=lr)
        else:
            opti = Adam(lr=lr)

        return opti

    def set_trainable(self, m, val):
        m.trainable = val
        for l in m.layers:
            l.trainable = val

    def _build_adversarial(self):

        # -------------------------------
        # Construct Computational Graph
        #       for the Critic
        # -------------------------------

        # Freeze generator's layers while training critic
        self.set_trainable(self.generator, False)

        # Image input (real sample)
        real_img = Input(shape=self.input_dim)

        # Fake image
        z_disc = Input(shape=(self.z_dim,))
        fake_img = self.generator(z_disc)

        # critic determines validity of the real and fake images
        fake = self.critic(fake_img)
        valid = self.critic(real_img)

        # Construct weighted average between real and fake images
        interpolated_img = RandomWeightedAverage(self.batch_size)([real_img, fake_img])
        print(interpolated_img.shape)

        # Determine validity of weighted sample
        validity_interpolated = self.critic(interpolated_img)

        # Use Python partial to provide loss function with additional
        # 'interpolated_samples' argument
        partial_gp_loss = partial(self.gradient_penalty_loss,
                                  interpolated_samples=interpolated_img)
        partial_gp_loss.__name__ = 'gradient_penalty'  # Keras requires function names

        self.critic_model = Model(inputs=[real_img, z_disc],
                                  outputs=[valid, fake, validity_interpolated])

        self.critic_model.compile(
            loss=[self.wasserstein, self.wasserstein, partial_gp_loss]
            , optimizer=self.get_opti(self.critic_learning_rate)
            , loss_weights=[1, 1, self.grad_weight]
        )

        # -------------------------------
        # Construct Computational Graph
        #         for Generator
        # -------------------------------

        # For the generator we freeze the critic's layers
        self.set_trainable(self.critic, False)
        self.set_trainable(self.generator, True)

        # Sampled noise for input to generator
        model_input = Input(shape=(self.z_dim,))
        # Generate images based of noise
        img = self.generator(model_input)
        # Discriminator determines validity
        model_output = self.critic(img)
        # Defines generator model
        self.model = Model(model_input, model_output)

        self.model.compile(optimizer=self.get_opti(self.generator_learning_rate)
                           , loss=self.wasserstein
                           )

        self.set_trainable(self.critic, True)

    def train_critic(self, x_train, using_generator):

        valid = np.ones((self.batch_size, 1), dtype=np.float32)
        fake = -np.ones((self.batch_size, 1), dtype=np.float32)
        dummy = np.zeros((self.batch_size, 1), dtype=np.float32)  # Dummy gt for gradient penalty

        if using_generator:
            true_imgs = next(x_train)[0]
            if true_imgs.shape[0] != self.batch_size:
                true_imgs = next(x_train)[0]
        else:
            idx = np.random.randint(0, x_train.shape[0], self.batch_size)
            true_imgs = x_train[idx]

        noise = np.random.normal(0, 1, (self.batch_size, self.z_dim))

        d_loss = self.critic_model.train_on_batch([true_imgs, noise], [valid, fake, dummy])
        return d_loss

    def train_generator(self):
        valid = np.ones((self.batch_size, 1), dtype=np.float32)
        noise = np.random.normal(0, 1, (self.batch_size, self.z_dim))
        return self.model.train_on_batch(noise, valid)

    def train(self, x_train, run_folder
              , n_critic=5
              , using_generator=False
              , save_sample_data=False
              , save_model_much=False):

        self.save_model(run_folder)
        
        for epoch in range(self.epoch, self.epoch + self.epochs):

            if epoch % 100 == 0:
                critic_loops = 5
            else:
                critic_loops = n_critic

            for _ in range(critic_loops):
                d_loss = self.train_critic(x_train, using_generator)

            g_loss = self.train_generator()

            print("%d (%d, %d) [D loss: (%.1f)(R %.1f, F %.1f, G %.1f)] [G loss: %.1f]" % (
            epoch, critic_loops, 1, d_loss[0], d_loss[1], d_loss[2], d_loss[3], g_loss))

            self.d_losses.append(d_loss)
            self.g_losses.append(g_loss)

            # If at save interval => save generated image samples
            if epoch % self.print_every_n_batches == 0:
                self.sample_images(run_folder, save_sample_data)
                self.model.save_weights(os.path.join(run_folder, 'weights/weights.h5'))
                if save_model_much:
                    self.save_model(run_folder)
                    self.model.save_weights(os.path.join(run_folder, 'weights/weights-%d.h5' % (epoch)))

            self.epoch += 1

        self.plot_hist(run_folder)

    def sample_images(self, run_folder, save_sample_data):

        r, c = 4, 4
        
        my_file = Path(run_folder + "/fixed_noise.npy")

        if my_file.is_file():
            print('fixed noise file found')
            noise = np.load(my_file)
        else:
            print('no fixed noise file')
            noise = np.random.normal(0, 1, (r * c, self.z_dim))
        
        gen_imgs = self.generator.predict(noise).squeeze()

        if self.opts['input_normalize_sym']:
            scaler = load(os.path.join(run_folder,'scaler.bin'))
            gen_imgs = scaler.inverse_transform(gen_imgs.reshape(gen_imgs.shape[0],gen_imgs.shape[1]*gen_imgs.shape[2])).reshape(gen_imgs.shape[0],gen_imgs.shape[1],gen_imgs.shape[2])
        
        if save_sample_data:
            pickle.dump(gen_imgs, open(os.path.join(run_folder, "images/sample_%d.pkl" % self.epoch), "wb"))
        
        fig, axs = plt.subplots(r, c, figsize=(8, 8))
        fig.subplots_adjust(left=0,right=1,bottom=0,top=1)
        fig.subplots_adjust(wspace=0, hspace=0)
        cnt = 0
        for i in range(r):
            for j in range(c):
                axs[i, j].plot(np.squeeze(gen_imgs[cnt, :, :]))
                #axs[i, j].axis('off')
                #axs[i, j].axis('tight')
                cnt += 1
        plt.savefig(os.path.join(run_folder, "images/sample_%d.png" % self.epoch))
        plt.close()

    def plot_model(self, run_folder):
        plot_model(self.model, to_file=os.path.join(run_folder, 'viz/model.png'), show_shapes=True,
                   show_layer_names=True)
        plot_model(self.critic, to_file=os.path.join(run_folder, 'viz/critic.png'), show_shapes=True,
                   show_layer_names=True)
        plot_model(self.generator, to_file=os.path.join(run_folder, 'viz/generator.png'), show_shapes=True,
                   show_layer_names=True)

    def plot_hist(self, run_folder):
        plt.figure(figsize=(8, 6))
        plt.plot([x[0] for x in self.d_losses], color='black', linewidth=0.5, label="Discriminator - Real")
        plt.plot([x[1] for x in self.d_losses], color='green', linewidth=0.5, label="Discriminator - Fake")
        plt.plot([x[2] for x in self.d_losses], color='blue', linewidth=0.5, label="Discriminator - Gradient")
        plt.plot([x for x in self.g_losses], color='red', linewidth=0.5, label="Generator")
        plt.xlabel('Epoch')
        plt.ylabel('Loss')
        plt.legend(loc='best')
        plt.savefig(os.path.join(run_folder, "loss_history.png"))
        plt.close()

    def save(self, folder):
        with open(os.path.join(folder, 'params.pkl'), 'wb') as f:
            pickle.dump(self.opts, f)
        self.plot_model(folder)

    def save_model(self, run_folder):
        pass
        self.save(run_folder)
        self.model.save(os.path.join(run_folder, 'model.h5'))
        self.critic.save(os.path.join(run_folder, 'critic.h5'))
        self.generator.save(os.path.join(run_folder, 'generator.h5'))
        pickle.dump(self, open(os.path.join(run_folder, "obj.pkl"), "wb"))

    def load_weights(self, filepath):
        self.model.load_weights(filepath)