import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm

def cov(x, y):
    xbar, ybar = x.mean(), y.mean()
    return np.sum((x - xbar) * (y - ybar)) / (len(x) - 1)

# Covariance matrix
def cov_mat(X):
    return np.array([[cov(X[:,0], X[:,0]), cov(X[:,0], X[:,1])], \
                     [cov(X[:,1], X[:,0]), cov(X[:,1], X[:,1])]])

class Sampler:
    """ To make a pdf then take a number of samples from the corresponding pdf
    """
    def __init__(self, mu, cov, xlim, ylim):
        self.mu = mu
        self.cov = cov
        self.xlim = xlim
        self.ylim = ylim
        self.lo = np.min(self.xlim + self.ylim)
        self.hi = np.max(self.xlim + self.xlim)

        assert(self.mu.shape[0] > self.mu.shape[1]), 'mu must be a row vector'
        assert(self.cov.shape[0] == self.cov.shape[1]), 'covariance matrix must be square'
        assert(self.mu.shape[0] == self.cov.shape[0]), 'cov_mat and mu_vec must have the same dimensions'

        self.pmax = self._pdf(np.array([[0],[0]]))

    def _pdf_from_2d(self, u, img, x, y):
        pass

    def _pdf(self, u):
        '''
        Caculate the multivariate normal density (pdf)

        Keyword arguments:
            u = numpy array of a "d x 1" sample vector
            mu = numpy array of a "d x 1" mean vector
            cov = "numpy array of a d x d" covariance matrix
        '''
        if u.shape == (1,2):
            u = u.reshape((2,1))
        assert(u.shape[0] > u.shape[1]), 'u must be a row vector'
        assert(self.mu.shape[0] == u.shape[0]), 'mu and u must have the same dimensions'

        part1 = 1 / ( ((2* np.pi)**(len(self.mu)/2)) * (np.linalg.det(self.cov)**(1/2)) )
        part2 = (-1/2) * ((u - self.mu).T.dot(np.linalg.inv(self.cov))).dot((u - self.mu))

        return float(part1 * np.exp(part2))

    def metropolis_hastings_sample(self, size=500000):
        burnin_size = 10000
        size += burnin_size
        x0 = np.array([[0, 0]])
        xt = x0
        samples = []
        for i in tqdm(range(size)):
            xt_candidate = np.array([np.random.multivariate_normal(xt[0], np.eye(2))])
            accept_prob = (self._pdf(xt_candidate)) / (self._pdf(xt))
            if np.random.uniform(0, 1) < accept_prob:
                xt = xt_candidate
            samples.append(xt)
        samples = np.array(samples[burnin_size:])
        samples = np.reshape(samples, [samples.shape[0], 2])
        return samples

def main():
    N = 256 * 256
    x = np.array([[0],[0]])
    mu1 = np.array([[0], [-0.5]])
    mu2  = np.array([[0.5],[0.5]])
    sx, sy = 0.08, 0.9
    scale = np.array(((sx, 0), (0, sy)))
    theta = -0.13 * np.pi
    c, s = np.cos(theta), np.sin(theta)
    rot = np.array(((c, -s), (s, c)))
    # Transformation matrix
    T = rot @ scale
    cov1 = T @ T.transpose()
    sx, sy = 0.2, 0.2
    scale = np.array(((sx, 0), (0, sy)))
    theta = 0.77 * np.pi
    c, s = np.cos(theta), np.sin(theta)
    rot = np.array(((c, -s), (s, c)))
    # Transformation matrix
    T = rot @ scale
    cov2 = T @ T.transpose()
    print('pdf cov : ',cov)
    xlim = np.array([[-5],[5]])
    ylim = np.array([[-5],[5]])

    sampler1 = Sampler(mu1, cov1, xlim, ylim)
    sampler2 = Sampler(mu2, cov2, xlim, ylim)
    #samples = sampler.sample(N) # much slower rejection sampling
    samples = sampler1.metropolis_hastings_sample(N)
    print(samples.shape)
    samples = np.append(samples,sampler2.metropolis_hastings_sample(N),axis=0)
    print(samples.shape)

    print('meas cov : ', cov_mat(samples))

    #debug cross check
    #samples = np.random.multivariate_normal([0,0], cov, N)

    mux = round(np.mean(samples[:,0]),4)
    muy = round(np.mean(samples[:,1]),4)
    sx = round(np.std(samples[:,0]),4)
    sy = round(np.std(samples[:, 1]), 4)

    plt.hexbin(samples[:, 0], samples[:, 1], cmap='rainbow')
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlim([-3, 3])
    plt.ylim([-3, 3])
    plt.title('means mux/muy ({}, {}), sigmas sx/sy ({}, {}). '.format(mux,muy,sx,sy))
    plt.show()

    xh = np.linspace(-3,3, 100)
    plt.hist(samples[:,0],xh,edgecolor='red', facecolor="None")
    plt.hist(samples[:,1],xh,edgecolor='blue', facecolor="None")
    plt.xlim(xlim)
    plt.grid(True)
    plt.show()

if __name__ == '__main__':
   main()